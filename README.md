# grunt-asset-compress

> Makes sure your asset_compress file has no duplicates!

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-asset-compress --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-asset-compress');
```

## The "asset_compress" task

### Overview
In your project's Gruntfile, add a section named `asset_compress` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
    asset_compress: {
        options: {
            src: './data/asset_compress.ini',
            groups: {
                my_group: ['my_file.js', 'other_file.js']
            }
        }
    }
});
```

### Options

#### options.src
Type: `String`

A string that provides the path to your `asset_compress.ini` file relative to the `Gruntfile.js` that is executing this task.

#### options.groups
Type: `Object`

An object that provides key-value pairs that resemble combinations of sections in your `asset_compress.ini` file. See the examples for more detail.

#### options.alias
Type: `Object`

An object with key-value pairs to replace the key with the value in each checked filepath.


#### options.generated
Type: `String[]`

An array of strings of files that are generated automatically during deployment. Use this to suppress errors about non-existent files; turning them into warnings.

### Usage Example

In this example, the `src` option clearly points to our `asset_compress.ini` file, but it could realistically point to any `.ini` file.

The `groups` option lists pages with their different themes. At the end of every chain you will have to provide a string to a section's name, or an array to several sections' names.


```js
grunt.initConfig({
    asset_compress: {
        options: {
            src: './data/asset_compress.ini',
            generated: [
                './build/application.min.js',
                './build/application.min.css'
            ],
            groups: {
                homepage: {
                    desktop: 'first_section.js',
                    mobile: ['first_section.js', 'second_section.js']
                },

                orderpage: {
                    cancel: {
                        desktop: 'third_section.js',
                        mobile: 'third_section.js'
                    },

                    pending: {
                        all: 'fifth_section.js'
                    },

                    confirm: {
                        desktop: ['third_section.js', 'fourth_section.js'],
                        mobile: ['second_section.js', 'third_section.js', 'fourth_section.js']
                    }
                }
            }
        }
    }
});
```

## Contributing
Whether you're a programmer or not, all contributions are very welcome! You could add features, improve existing features or request new features. Assuming the unit tests cover all worst-case scenarios, you will not be able to report bugs because there will be no bugs.

If you want to make changes to the source, you should fork this repository and create a pull-request to our master branch. Make sure that each individual commit does not break the functionality, and contains new unit tests for the changes you make. Existing assertions will not be edited until a major release to remain compatible with older versions, so please do not change them unless absolutely necessary.

To test your changes locally, run `npm install` followed by `npm test`.

## Versioning
As much as we want everyone to always use the latest version, we know that this is a utopia. Therefore, we adhere to a strict versioning system that is widely accepted: `major.minor.patch`. This is also known as the [SemVer](http://semver.org/spec/v2.0.0.html) method.
