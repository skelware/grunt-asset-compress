/*
 * To test, run `grunt` in this directory.
 *
 * Expected result for `grunt`:
 * >> "orderpage.pending.all": Could not find section "fifth_section.js"
 * >> "orderpage.confirm.mobile": Duplicate definition of "file1" in sections "third_section.js" and "second_section.js"
 * Warning: Finished with a total of 2 errors and 1 warnings! Use --force to continue.
 *
 * Expected result for `grunt --verbose`
 * >> "orderpage.cancel.mobile": Duplicate of "orderpage.cancel.desktop"
 * >> "orderpage.pending.all": Could not find section "fifth_section.js"
 * >> "orderpage.confirm.mobile": Duplicate definition of "file1" in sections "third_section.js" and "second_section.js"
 * Warning: Finished with a total of 2 errors and 1 warnings! Use --force to continue.
 */
module.exports = function(grunt) {
    'use strict';

    grunt.loadTasks('tasks');
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        eslint: {
            options: {
                config: '.eslintrc'
            },
            target: 'tasks/'
        },

        asset_compress: {
            options: {
                src: './data/asset_compress.ini',
                alias: {
                    'WEBROOT': 'build'
                },
                generated: [
                    'application.min.js'
                ],
                groups: {
                    homepage: {
                        desktop: 'application.js', //good
                        mobile: ['application.js', 'plugin.js'] //conflict
                    },

                    orderpage: {
                        cancel: {
                            desktop: ['empty.js', 'application.js'], //good
                            mobile: 'duplicate.js' //duplicate
                        },

                        pending: {
                            all: ['application.js', 'duplicate.js', 'nonexistent.js'] //duplicate, section does not exist
                        }
                    }
                }
            }
        }
    });

    grunt.registerTask('default', ['eslint', 'asset_compress']);
};
